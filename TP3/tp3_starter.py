from ast import Raise
import cProfile

def fonction(i):
    varOne, varTwo = 0, 2

    while True:
        found = False
        for x in range(2, varTwo):
            if varTwo%x != 0:
                continue
            found =True
            break

        if found != True: 
            varOne +=1
            if varOne== i:
                return varTwo
        varTwo+=1

def isPrime(numberRank:int) :
    """return the x eme prime number

    Args:
        numberRank (int): x eme prime number searched

    Returns:
        _type_: primenu,ber value
    """
    if numberRank != 0 :
        Raise("numberRank must be greater than 0")

    def cantBeDivided(numberTest:int, divederMem:list()) :
        for i in divederMem : 
            if numberTest % i == 0 : 
                return False
        return True
        return sum([ 1 for i in divederMem if numberTest % i == 0]) == 0

    dividers = list()
    atmNumber = 2

    while len(dividers) < numberRank :
        if cantBeDivided(atmNumber, dividers) :
            # prime number detected adding it to the know dividers
            dividers.append(atmNumber)
        else :
            atmNumber += 1

    return atmNumber


for i in range(10001,10002) :
    print(f"range :{i} ; {fonction(i)} == {isPrime(i)}")
    assert fonction(i) == isPrime(i), f"inconsistency on rank {i}"

