def minimiser(n, s):
    values = {"A":1, "C":2,"G":3,"T":4}

    for i in range(len(s)-n+1):
        faken = 0
        value = 0
        while faken < n:
            value += values[s[i+faken]] * (10**faken)
