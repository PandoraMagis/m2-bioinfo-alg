import random
import time
from dynamicProg import DynamicMatrix


# myMat = DynamicMatrix("GGATAGC", "AATGAATC", +2, -1, -2)

# myMat.printMatrix()


seqOne = "".join([ random.choice("ACGT") for i in range(1000)])
seqTwo = "".join([ random.choice("ACGT") for i in range(1000)])

myMat = DynamicMatrix(seqOne, seqTwo, +2, -1, -2)

testWidness = [1, 5, 10, 20, 40, -1]

for i in testWidness:
    myTime = -time.time()
    myMat = DynamicMatrix(seqOne, seqTwo, +2, -1, -2, i)
    myTime += time.time()
    print(f"widness: {i} \t score: {myMat.matrix[-1][-1]} \t time: {myTime}")

    