#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from turtle import width


class DynamicMatrix:
    '''stores a matrix |S|x|T| (|S|+1 lines and |T|+1columns), sequences S and T and the score system (match, mismatch, gap)
        defines some global alignment functions
        '''
    def __init__(self, S, T, match, mismatch, gap, width = -1):
        ''' defines and stores initial values'''
        
        self.S=S
        self.T=T
        self.gap=gap
        self.match=match
        self.mismatch=mismatch

        self.windth = width
        
        self.matrix = [0 for i in range(len(S)+1)]
        for i in range(len(S)+1):
            self.matrix[i] = [0 for j in range(len(T)+1)]  # type: ignore
        self.initGobal()
        self.fillMatrix()
        # self.printGlobalAllignment()

    def score(self, charOne, charTwo) :
        return self.match if charOne == charTwo else self.mismatch

    def initGobal(self):
        '''initializes the matrix for global alignment'''
        for i in range(1,len(self.S)+1):
            self.matrix[i][0] = self.matrix[i-1][0] + self.gap # type: ignore
        for j in range(1,len(self.T)+1):
            self.matrix[0][j] = self.matrix[0][j-1] + self.gap # type: ignore

    def printGlobalAllignment(self):
        ''' prints the global alignment'''
        i = len(self.S)
        j = len(self.T)
        alignS = ""
        alignT = ""
        match = 0

        while i > 0 and j > 0:
            if self.matrix[i][j] == self.matrix[i-1][j-1] + self.match:
                alignS = self.S[i-1] + alignS
                alignT = self.T[j-1] + alignT
                i -= 1
                j -= 1
                match += 1
            elif self.matrix[i][j] == self.matrix[i-1][j-1] + self.mismatch:
                alignS = self.S[i-1] + alignS
                alignT = self.T[j-1] + alignT
                i -= 1
                j -= 1
            elif self.matrix[i][j] == self.matrix[i-1][j] + self.gap:
                alignS = self.S[i-1] + alignS
                alignT = "-" + alignT
                i -= 1
            else:
                alignS = "-" + alignS
                alignT = self.T[j-1] + alignT
                j -= 1
        while i > 0:
            alignS = self.S[i-1] + alignS
            alignT = "-" + alignT
            i -= 1
        while j > 0:
            alignS = "-" + alignS
            alignT = self.T[j-1] + alignT
            j -= 1
        print(alignS)
        print(alignT)
        print(f"Pid: {match/(len(alignS)-1)}")

    def fillMatrix(self):
        ''' fills the matrix'''
        if self.windth == -1:
            for i in range(1,len(self.S)+1):
                for j in range(1,len(self.T)+1):
                    self.matrix[i][j] = max(self.matrix[i-1][j-1] + self.score(self.S[i-1], self.T[j-1]), # type: ignore
                                            self.matrix[i-1][j] + self.gap,
                                            self.matrix[i][j-1] + self.gap)  
        else: 
            for i in range(1,len(self.S)+1):
                # for j in range(1,len(self.T)+1):
                for j in range(i-self.windth, i+self.windth+1):
                    if not 0 < j < len(self.T)+1: continue
                    # if (i - j) > self.windth or (j - i) > self.windth :
                    #     continue
                    self.matrix[i][j] = max(self.matrix[i-1][j-1] + self.score(self.S[i-1], self.T[j-1]), # type: ignore
                                            self.matrix[i-1][j] + self.gap,
                                            self.matrix[i][j-1] + self.gap)   
    def printMatrix(self):
        ''' prints the matrix'''
        width = 4
        vide = " "
        line = f"{vide:>{2*width}}"
        for j in range(0,len(self.T)):
            line += f"{self.T[j]:>{width}}"
        print(line)
        line = f"{vide:>{width}}"
        for j in range(0,len(self.T)+1):
            line += f"{self.matrix[0][j]:>{width}}"
        print(line)
        for i in range(1,len(self.S)+1):
            line = f"{self.S[i-1]:>{width}}"
            for j in range(0,len(self.T)+1):
                line += f"{self.matrix[i][j]:>{width}}"
            print(line)
            

# dm = DynamicMatrix("GGATAGC", "AATGAATC", +2, -1, -1)
# dm.printMatrix()
