from tools_karkkainen_sanders import simple_kark_sort


MAX_STR_SIZE = 40

isNucleo = lambda x : x.strip(["ACTG"]) == "", "bad"

def get_bwt(s:str, sa:str) :
    """ the result is the last char of every suffixes sorted in alphnumeric numbers
    but ye use this way cause this implementation is not eq to the realitty yhere we work with multiple
    lines and where a sort will kill out the process in temrs of time
    """
    bwt = ""
    for i in range(len(s)):
        if sa[i]==0: # i.e. end of treatement
            bwt += "$"
        else:
            bwt += s[sa[i]-1]
    return bwt
    
def get_n(bwt) :
    """_summary_

    Args:
        bwt (_type_): _description_

    Returns:
        _type_: _description_
    """
    crossed, rtn = "", {}

    for count, char in enumerate(sorted(bwt)) :
        if char not in crossed :
            rtn[char] = count
        else : 
            crossed += char
    return rtn

def get_r(bwt):
    rtn, last = {}, 0
    ns = get_n(bwt)

    for char in ns :
        if char not in rtn :
            rtn[char] = last
            last += ns[char]
            if last == 0 :last += 1
    return rtn

def left_first(a, k, rRanks) :
    return rRanks[a] + k - 1

def bwt_2_seq(bwt, n, r) :
    print(r)
    print(n)
    print(bwt)
    left_first( n, 1, r )

if __name__ == '__main__':
    def splitKarkkainen(s) :
        kark = simple_kark_sort(s)
        for i in range(len(s)):
            print(f'{i}\t{kark[i]}\t{s[kark[i]]}\t{s[kark[i]:][:MAX_STR_SIZE]}')
        sa, f, suffixes = [], [], []
        for i in range(len(s)):
            sa.append(kark[i])
            f.append(s[kark[i]])
            suffixes.append(s[kark[i]:][:MAX_STR_SIZE])

        return (sa, f, suffixes)


    s = 'GGCGGCACCGC$'
    sa, f, sufx = splitKarkkainen(s)

    bwt = get_bwt(s, sa)

    # nbCHarLowerThan = get_n(bwt)

    # print(nbCHarLowerThan)

    # print(get_r(bwt))

    bwt_2_seq(bwt, get_n(bwt) ,get_r(bwt))
