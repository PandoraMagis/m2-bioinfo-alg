from OrganizerMethod import OrganizerMethod


class implemMy(OrganizerMethod):
    def __init__(
        self,
        fileName,
        outputFileName=None,
        minimiserSize=10,
        stopWhenOneCommonMin=True,
        KmersMinSize=10,
    ):
        super().__init__(
            fileName,
            outputFileName,
            minimiserSize,
            stopWhenOneCommonMin,
            KmersMinSize,
        )
        self.KmersMinSize = KmersMinSize
        self.createKmersDict(
            self.KmersMinSize,
        )

    def doTheTrick(self):
        # TODO : implement the algorithm
        pass

    def createKmersDict(self, kmerMinSize: int, kmerMaxSize: int) -> dict:
        """Create a dictionary of all kmers of size between kmerMinSize and kmerMaxSize
        Args:
            kmerMinSize (int): minimum size of the kmers
            kmerMaxSize (int): maximum size of the kmers
        Returns:
            dict: dict of kmers
        """
        # TODO : implement the algorithm

        self.kmersGroupsDict = {i: {} for i in range(kmerMinSize, kmerMaxSize + 1)}

        for lineNumber, line in enumerate(self.LinesGenerator):

            for kmerSize in range(kmerMinSize, kmerMaxSize + 1):
                # case kinda suprinsing
                if kmerSize > len(line):
                    continue

                actualKmer = line[:kmerSize]
                kmerCount, kmerLines = self.kmersGroupsDict[kmerSize].get(
                    actualKmer, (0, [])
                )

                self.kmersGroupsDict[kmerSize][actualKmer] = (
                    kmerCount + 1,
                    kmerLines + [lineNumber],
                )

                # kmersGroupsDict[kmerSize][line[:kmerSize]] = kmerValInDict
                # kmersGroupsDict[kmerSize][line[:kmerSize]] = (
                #     [lineNumber]
                #     if line[0:kmerSize] not in kmersGroupsDict[kmerSize]
                #     else kmersGroupsDict[kmerSize][line[0:kmerSize]] + [lineNumber]
                # )

        # for kmerLen in kmersGroupsDict:
        #     for kmerCount, kmerLines in kmersGroupsDict[kmerLen].values():
        #         print(kmerCount)
        # print(kmersGroupsDict[kmerLen][kmer])
        # for i in kmersGroupsDict[kmerLen][kmer]:
        #     print(i)

        kmerGroupNumber = {
            kmerLen: [
                kmerCount
                for kmerCount, kmerLines in self.kmersGroupsDict[kmerLen].values()
            ]
            for kmerLen in self.kmersGroupsDict
        }
        print(kmerGroupNumber)
        self.prettyPrintKmerGroup()

    def deleteUselessChilds(self):
        """Delete the childs of the nodes I.E nodes wiuth aprent already =1"""

        for kmerLen in self.kmersGroupsDict:
            knowedParents = set()

            for kmer in self.kmersGroupsDict[kmerLen]:
                kmerCount, kmerLines = self.kmersGroupsDict[kmerLen][kmer]
                if kmerCount == 1:
                    knowedParents.add(kmer)
                else:
                    for i in range(len(kmer)):
                        if kmer[:i] in knowedParents:
                            self.kmersGroupsDict[kmerLen][kmer] = (0, [])
                            break

    def prettyPrintKmerGroup(self):
        maxKmerSize = max(
            [
                kmerLen
                for kmerLen in self.kmersGroupsDict
                if len(self.kmersGroupsDict[kmerLen]) > 1
            ]
        )
        dispatchLow = lambda kmerLen: [
            f"{kmer} - {self.kmersGroupsDict[kmerLen][kmer][0]}"
            for kmer in self.kmersGroupsDict[kmerLen]
        ]

        def dispatch(kmerLen, strSize: int):
            line = dispatchLow(kmerLen)
            return " ".join([f"{i}{' '*(strSize-len(i))} " for i in line])

        kmerGroupPrettyPrint = "".join(
            [
                f"\nkmerLen = {kmerLen} :\t {dispatch(kmerLen, maxKmerSize+1+3)}"
                for kmerLen in self.kmersGroupsDict
                if len(self.kmersGroupsDict[kmerLen]) > 1
            ]
        )
        print(kmerGroupPrettyPrint)


implemMy("test/gzipfiletest.txt").createKmersDict(1, 15)
# implemMy("test/ecoli_100Kb_reads_5x.fasta").createKmersDict(1, 4)
