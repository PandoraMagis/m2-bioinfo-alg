from OrganizerMethod import OrganizerMethod
from ressources import minimiser
import os

class VortexMethod(OrganizerMethod):
    def __init__(
        self,
        fileName,
        outputFileName=None,
        minimiserSize=10,
        vortexWindowSize=30,
        vortexSize=10,
        naive=False
    ):
        super().__init__(
            fileName,
            outputFileName
        )
        
        # vortex word size
        self.vortexSize: int = vortexSize
        self.vortexWindowSize: int = vortexWindowSize

        self.minimiserSize: int = minimiserSize

        self.naiveMethod: bool = naive


    def reorganiseLines(self) -> list:
        """Reorganise the lines of the file linked to this class using the lexicographic minimiser method.
        
            Returns:
                A list of the reorganised line indexes."""
        i = 0

        currentList = [] # current line order
        finalList = [] # Final line order

        while(i < self.lineLength - self.vortexSize):
            j = 0
            wordDict = {} # Dict of all kmers key : the kmer; value : list of line index with this kmer in the interval [i:i+self.vortexSize]
            for line in self.LinesGenerator:
                if j not in finalList:
                    if not self.naiveMethod:
                        # Use the vortex with minimiser improvement
                        readIdentifier = minimiser(line[i:i+self.vortexWindowSize], self.minimiserSize)
                    else:
                        # Use the naive vortex method
                        readIdentifier = line[i:i+self.vortexSize]
                    wordDict[readIdentifier] = [j] if readIdentifier not in wordDict else wordDict[readIdentifier] + [j]
                j += 1
            
            # get the key with the maximum length 
            maxlen = max(len(wordDict[word]) for word in wordDict)
            key = [word for word in wordDict if len(wordDict[word]) == maxlen][0]

            LinesOfMostCommonWord = wordDict[key]

            templist = []
            for lineIndex in LinesOfMostCommonWord:
                if currentList and lineIndex in currentList: # If it is a lineIndex we already know, it must be moved at the end of the current list
                    currentList += [currentList.pop(currentList.index(lineIndex))]
                else:
                    templist += [lineIndex]

            finalList += currentList
            currentList = templist
            i += self.vortexSize
            self.LinesGenerator = self.getLine()

        # Finally, add the last current list and all the non-sorted lines
        finalList += currentList
        finalList += sorted(set(range(0, j)) - set(finalList))
        return finalList
                

    def writeReorganisedLines(self, reorganised_index):
        readHolder = [None] * len(reorganised_index)
        with open(self.outputFileName, "w") as f:
            i = 0
            lastWritten = 0
            for line in self.LinesGenerator:
                #print("stocking line", line)
                index = reorganised_index.index(i)
                readHolder[index] = line
                while lastWritten < len(readHolder) and readHolder[lastWritten]:
                    f.write(f"{readHolder[lastWritten]}\n")
                    #print("written line", readHolder[lastWritten], "readHolder has", sum([1 for item in readHolder if item]))
                    readHolder[lastWritten] = None
                    lastWritten += 1
                i += 1