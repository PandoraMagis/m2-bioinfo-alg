from OrganizerMethod import OrganizerMethod
from pathlib import Path
import time
import matplotlib.pyplot as plt
import gzip

methods = ["lexmin"]

covers = [5,40,120]
order = ["ecoli (40x)","human (40x)","metagenome"]

sizes = [3.298425776395004, 3.845738751359472,2.7115948242762435]
times = [11.77, 132.22, 377.02]

plt.subplot(1,2,1)
plt.bar(order, sizes)
plt.ylabel("Compression ratio")
plt.subplot(1,2,2)
plt.bar(order, times)
plt.ylabel("Duration (s)")


# for method in methods:
# size = []
# duration = []
# ram = []
# for cover in covers:
#     with open(f"projet/graph_material/kmersplit_coli_sizes.txt") as sf:
#         size.append([float(line.rstrip()) for line in sf])
#     with open(f"projet/graph_material/kmersplit_coli_times.txt") as df:
#         duration.append([float(line.rstrip()) for line in df])
#     with open(f"projet/graph_material/kmersplit_coli_ram.txt") as rf:
#         ram.append([float(line.rstrip()) for line in rf])
# x = [5,10,20,40,80,120]
# p1 = plt.subplot(2,2,1)
# for i in range(len(size)):
#     plt.plot(x, size[i])
# plt.ylabel('Compression ratio')
# plt.xlabel('Cover')
# plt.grid(ls="--")


# p2 = plt.subplot(2,2,2)
# for i in range(len(size)):
#     plt.plot(x, duration[i])
# plt.ylabel('Duration (s)')
# plt.xlabel('Cover')
# plt.grid(ls="--")


# p3 = plt.subplot(2,2,3)
# for i in range(len(size)):
#     plt.plot(x, ram[i])
# plt.ylabel('RAM usage')
# plt.xlabel('Cover')
# plt.grid(ls="--")


# plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.) #add the legend (corresponding to the labels above)


plt.show() #display the figure
        

# paths = ["ecoli_100Kb_reads_5x.fasta", "humch1_1Mb_reads_5x.fasta"]

# duration = []
# resource_cons = []

# for path in paths:
#     time_start = time.perf_counter()
    
#     readOrg = OrganizerMethod(Path("test",path))
#     readOrg.doTheTrick()

#     time_elapsed = (time.perf_counter() - time_start)
#     memMb=resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1024.0/1024.0

#     duration.append(time_elapsed)
#     resource_cons.append(memMb)

