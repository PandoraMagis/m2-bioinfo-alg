i=7
touch sizes.txt
file=test/ecoli_100Kb_reads_120x.fasta
gzip -fk $file

while [[ $i -le 15 ]]; do
    echo "Testing word size $i"
    rm -f ${file}_reorg.gz
    python read_organizer.py -i $file -w $i
    echo "compressing file"
    gzip -k ${file}_reorg
    echo "comparating file sizes"
    ./size_calculator.sh $file.gz ${file}_reorg.gz >> sizes.txt
    echo "Next !"
    ((i++))
done