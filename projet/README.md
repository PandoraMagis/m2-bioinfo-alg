# Read Organizer
### Better compression using gzip

This is a fairly simple program that sorts a .fasta file to improve the compression with gzip. The compression is not lossless, as fasta headers and read order are lost during file treatment.

## Usage

To use the program, enter the command

```py
./read_organizer.py -i input_file -o output_file
```

The following method can be used to sort the reads:

* `-l` or `--method LexMin` : Lexicographic minimiser (Default). Sort recursively the reads based on their minimisers.

    * `-c nb_reads` : Sets the cutoff of how many reads can be in a group before the recursion stops. Default is 10.

    * `-s True/False` : Precise whether the recursion stops when there is only one minimiser in a group. Default is True.

    * `-w minimiserSize` : Sets the minimiser size. Default is 8.

* `-v` or `--method Vortex` : Vortex method. Uses vortex sort sequentially.

    * `-w vortexSize` : Sets the vortex kmer size. Default is 6.

* `--vm` or `--method VortexMin` : Vortex method improved to sort reads by minimiser in a vortex window.

    * `-w minimiserSize` : Sets the vortex kmer size. Default is 6.

    * `--vortex-window windowSize` : Sets the size of the vortex to search the minimiser in. Default is 50.

* `-k` or `--method KmerSplit` : Kmer Splitter method. Dynamically finds the best parameters to sort the reads by their kmers.