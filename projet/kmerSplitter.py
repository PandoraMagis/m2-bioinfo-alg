from pathlib import Path
from OrganizerMethod import OrganizerMethod


class KmerSplitter(OrganizerMethod):
    """This class is an implementation of the OrganizerMethod class
    and aim at implementing the algorithm to reorginise a file before gzip compression.

    To do so the main algorythm find the best kmersLengt, to a point were one kmezr is
    almost equals to one line. Not all line will be equal to one kmer, these who remains linked
    to a single kmer will be sub sorted depending on the next characters.

    Args:
        OrganizerMethod (_type_): _description_
        outputFileName (Path, optional): [description]. Defaults to None.
        kmersMinSize (int, optional): [description]. Defaults to 2.
        kmerMaxSize (int, optional): [description]. Defaults to 10.
    """

    def __init__(
        self,
        fileName: Path,
        outputFileName=None,
        kmersMinSize: int = 2,
        kmerMaxSize: int = 10,
    ):
        super().__init__(
            fileName,
            outputFileName,
        )
        self.kmersMinSize = kmersMinSize
        self.kmersMaxSize = kmerMaxSize

    def reorganiseLines(self):

        # implemMy("test/gzipfiletest.txt").createKmersDict(1, 15)
        # len kmers to be tested
        minKmerSize = self.kmersMinSize
        maxKmerSize = self.kmersMaxSize
        self.createKmersDict(self.kmersMinSize, self.kmersMaxSize)
        # self.ratioOneManyKmers()
        bestRatioIndex = self.findBestLen()
        print(f"bestRatioIndex : {bestRatioIndex}")

        ones, many, magic = self.splitOneMany(bestRatioIndex)

        # self.printKmersGroupsNumber(bestRatioIndex)

        while bestRatioIndex >= self.kmersMaxSize:
            # reset Lines reading
            self.LinesGenerator = self.getLine()

            minKmerSize += self.kmersMinSize
            maxKmerSize += self.kmersMaxSize
            kmerStartingChar = minKmerSize
            self.createKmersDict(minKmerSize, maxKmerSize, many, kmerStartingChar)

            ones, many, magic = self.splitOneMany(bestRatioIndex)

        self.LinesGenerator = self.getLine()

        # self.splitOneMany(bestRatioIndex + kmerStartingChar + minKmerSize)

        # lineToNewKmer = self.reverseDict()
        print("last step")

        list.sort(magic, key=lambda x: x[0])

        sortedLinesNumbers = []

        def subSort(l):
            if isinstance(l, list):
                l.sort()
            return l

        magic = [(kmerCount, subSort(kmerLines)) for kmerCount, kmerLines in magic]

        for _, lines in magic:
            if isinstance(lines, list):
                for i in lines:
                    sortedLinesNumbers.append(i)
            else:
                sortedLinesNumbers.append(lines)

        # print(len(sortedLinesNumbers))

        print("sort done")

        return sortedLinesNumbers

        # self.compareCompress()
        # # TODO : implement the algorithm
        # pass

    def createKmersDict(
        self,
        kmerMinSize: int,
        kmerMaxSize: int,
        linesRestrict: list = [],
        seqcharignore: int = 0,
    ) -> dict:
        """Create a dictionary of all kmers of size between kmerMinSize and kmerMaxSize
        Args:
            kmerMinSize (int): minimum size of the kmers
            kmerMaxSize (int): maximum size of the kmers
        Returns:
            dict: dict of kmers
        """
        # TODO : implement the algorithm

        kmerMinSize, kmerMaxSize = (
            seqcharignore + kmerMinSize,
            seqcharignore + kmerMaxSize,
        )
        # stop contition
        if kmerMinSize >= kmerMaxSize:
            return

        self.kmersGroupsDict = {i: {} for i in range(kmerMinSize, kmerMaxSize + 1)}

        for lineNumber, line in enumerate(self.LinesGenerator):

            if len(linesRestrict) == 0 or lineNumber in linesRestrict:
                for kmerSize in range(kmerMinSize, kmerMaxSize + 1):
                    # case kinda suprinsing
                    if kmerSize > len(line):
                        continue

                    actualKmer = line[:kmerSize]
                    kmerCount, kmerLines = self.kmersGroupsDict[kmerSize].get(
                        actualKmer, (0, [])
                    )

                    self.kmersGroupsDict[kmerSize][actualKmer] = (
                        kmerCount + 1,
                        kmerLines + [lineNumber],
                    )

                    # kmersGroupsDict[kmerSize][line[:kmerSize]] = kmerValInDict
                    # kmersGroupsDict[kmerSize][line[:kmerSize]] = (
                    #     [lineNumber]
                    #     if line[0:kmerSize] not in kmersGroupsDict[kmerSize]
                    #     else kmersGroupsDict[kmerSize][line[0:kmerSize]] + [lineNumber]
                    # )

        # for kmerLen in kmersGroupsDict:
        #     for kmerCount, kmerLines in kmersGroupsDict[kmerLen].values():
        #         print(kmerCount)
        # print(kmersGroupsDict[kmerLen][kmer])
        # for i in kmersGroupsDict[kmerLen][kmer]:
        #     print(i)

        # print(kmerGroupNumber)
        # self.prettyPrintKmerGroup()

    def deleteUselessChilds(self):
        """Delete the childs of the nodes I.E nodes wiuth aprent already =1"""

        for kmerLen in self.kmersGroupsDict:
            knowedParents = set()

            for kmer in self.kmersGroupsDict[kmerLen]:
                kmerCount, kmerLines = self.kmersGroupsDict[kmerLen][kmer]
                if kmerCount == 1:
                    knowedParents.add(kmer)
                else:
                    for i in range(len(kmer)):
                        if kmer[:i] in knowedParents:
                            self.kmersGroupsDict[kmerLen][kmer] = (0, [])
                            break

    def printKmersGroupsNumber(self, index=None):
        """Print the kmersGroupsDict"""
        kmerGroupNumber = {
            kmerLen: [
                kmerCount
                for kmerCount, kmerLines in self.kmersGroupsDict[kmerLen].values()
            ]
            for kmerLen in self.kmersGroupsDict
            # if 2 < len(kmerLen) < 5
        }
        if index is None:
            print(kmerGroupNumber)
        else:
            print(kmerGroupNumber[index])

    def prettyPrintKmerGroup(self):
        maxKmerSize = max(
            [
                kmerLen
                for kmerLen in self.kmersGroupsDict
                if len(self.kmersGroupsDict[kmerLen]) > 1
            ]
        )
        dispatchLow = lambda kmerLen: [
            f"{kmer} - {self.kmersGroupsDict[kmerLen][kmer][0]}"
            for kmer in self.kmersGroupsDict[kmerLen]
        ]

        def dispatch(kmerLen, strSize: int):
            line = dispatchLow(kmerLen)
            return " ".join([f"{i}{' '*(strSize-len(i))} " for i in line])

        kmerGroupPrettyPrint = "".join(
            [
                f"\nkmerLen = {kmerLen} :\t {dispatch(kmerLen, maxKmerSize+1+3)}"
                for kmerLen in self.kmersGroupsDict
                if len(self.kmersGroupsDict[kmerLen]) > 1
            ]
        )
        print(kmerGroupPrettyPrint)

    def ratioOneManyKmers(self, debug=False):
        """Return the ratio of one to many kmers"""
        ratios = []
        for kmerLen in self.kmersGroupsDict:
            oneKmers = 0
            manyKmers = 0
            for kmerCount, kmerLines in self.kmersGroupsDict[kmerLen].values():
                if kmerCount == 1:
                    oneKmers += 1
                else:
                    manyKmers += 1

            kmerRatio = 0
            if oneKmers > 1:
                kmerRatio = manyKmers / oneKmers
            if debug:
                print(f"ratio of one to many kmers for kmerLen {kmerLen} : {kmerRatio}")
                print(f"oneKmers = {oneKmers} manyKmers = {manyKmers}\n")
            ratios.append(kmerRatio)
        return ratios

    def findBestLen(self, debug=False):
        """Return the best kmer length"""
        ratios = self.ratioOneManyKmers()
        proge = []

        for i in range(len(ratios) - 1):
            if ratios[i] > 0:
                thisRatio = ratios[i + 1] / ratios[i]
                if debug:
                    print(f"ratio rank {i}= {thisRatio}")
                # print(f"{i} - {thisRatio}")

                if len(proge) > 1 and thisRatio - proge[-1] < 0.05:
                    return i
                proge.append(thisRatio)

        # print(
        #     "Kmer best length for reducing not found, consider using a bigger kmer size"
        # )
        # no best index found
        return len(ratios) - 1

    def splitOneMany(self, kmerLen: int):
        """Split the kmersGroupsDict in two parts : one and many"""
        kmerLen = self.kmersMinSize + kmerLen
        oneKmers = []
        manyKmers = {}

        for kmer in self.kmersGroupsDict[kmerLen]:
            kmerCount, kmerLines = self.kmersGroupsDict[kmerLen][kmer]
            if kmerCount == 1:
                oneKmers.append((kmer, kmerLines[0]))
            else:
                manyKmers[kmer] = (kmer, kmerLines)
                # TODO : magic trick append isn't great way go dsict and add to dict
                # magicTrick.append((kmer, kmerLines))
        return (
            oneKmers,
            manyKmers,
            [manyKmers[kmer] for kmer in manyKmers] + oneKmers,
        )

    def reverseDict(self):
        # should,n't be used anymore
        lineToKmer = {}
        for kmerLen in self.kmersGroupsDict:
            for kmer in self.kmersGroupsDict[kmerLen]:
                kmerCount, kmerLines = self.kmersGroupsDict[kmerLen][kmer]
                for line in kmerLines:
                    lineToKmer[line] = kmer
        return lineToKmer


# print(many)

# ecoli.printKmersGroupsNumber()

# ecoli.prettyPrintKmerGroup()
# a.prettyPrintKmerGroup()
# a.printKmersGroupsNumber()

# # for kmer, lines in magic:
# #     if isinstance(lines, list):
# #         lineNewKmer =
# #         list.sort(lineNewKmer, key=lambda x: x[0])
# #         # print(lines)

# # magic = [
# #     (kmer, lines)
# #     if not isinstance(lines, list)
# #     else (kmer, [line for line in lines].sort())
# #     for kmer, lines in magic
# # ]

# sortedLinesNumbers = [i[1] for i in magic]
# for i in sortedLinesNumbers:
#     if isinstance(i, list):
#         continue
#         print(i)
#     else:
#         continue
#         print(i)
# print(sortedLinesNumbers)
# sort all kmers with one only and reccur to sort remainning kmers
# TODO  : kmer Min et max size to add as class attribute

