# Find the minimiser of the sequence, i.e. the [order]th of all kmers sorted in lexicographic order.
minimiser = lambda seq, size, order=0: sorted(
    ["".join(seq[i : i + size]) for i in range(len(seq) - size + 1)]
)[order]

import time
import os


def getRam() -> float:
    """Gets the memory usage of this program. Works on Windows and Unix."""
    try:
        import resource
        return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024.0 / 1024.0
    except ModuleNotFoundError:
        import psutil
        pid = os.getpid()

# Get the process object
        process = psutil.Process(pid)

        # Get the memory usage of the process in bytes
        memory_usage = process.memory_info().rss

        # Convert the memory usage from bytes to megabytes
        return memory_usage / 1024 / 1024

# This class allows us to monitor the time spent by a function
class Benchmarker:
    # Function called right before the execution of the function
    def __enter__(self):
        self.t1 = time.perf_counter()
        return self

    # Function called right after the function
    def __exit__(self, type, value, traceback):
        self.t2 = time.perf_counter()
        self.t = self.t2 - self.t1
        self.ram = getRam()

    # Function that prints on the shell the time spent by the instructions
    def results(self, template: str = "{}\n{}"):
        return template.format(round(self.t, 2), self.ram)

