import os
import gzip
from pathlib import Path
from typing import Generator
from abc import ABC, abstractmethod

# from ressources import minimiser


class OrganizerMethod(ABC):
    def __init__(self, fileName, outputFileName=None):
        self.fileName: Path = fileName
        self.LinesGenerator: Generator = self.getLine()
        self.outputFileName: Path = (
            outputFileName if outputFileName != None else Path(f"{self.fileName}_reorg")
        )
        self.compressedOutputFileName: Path = Path(f"{self.outputFileName}.gz")
        self.lineLength = len(next(self.getLine()))

    ##### Informations Getters #####

    def getFileSize(self, fileName=None):
        """Get the file size in bytes"""
        fileName = fileName if fileName != None else self.fileName
        return os.path.getsize(fileName)

    def getFileLines(self, fileName=None):
        """Get the file number of lines"""
        fileName = fileName if fileName != None else self.fileName
        return sum(1 for line in self.LinesGenerator)

    def getLine(self, fileName=None, openArgs="r") -> Generator:
        """return a generator of lines from the file
        Args:
            openArgs (str, optional): opening type file, don't forget the 'r'. Defaults to "r".
        Yields:
            str | bytes: Lines of the file linked to this class
        """

        # TODO : add a check for the openArgs, should be in in any case and w or a souldn't be

        fileName = fileName if fileName != None else self.fileName
        with open(fileName, openArgs) as f:
            for line in f:
                if openArgs == "r" and line.startswith(">"):
                    continue
                else:
                    yield line.rstrip()

    ##### Methods #####

    def compressFile(self, fileName=None, outputFileName=None):
        """Compress the file linked to this class"""
        if fileName is None:
            fileName = self.outputFileName
        if outputFileName is None:
            outputFileName = self.compressedOutputFileName

        with open(fileName, "rb") as f_in, gzip.open(outputFileName, "wb") as f_out:
            f_out.writelines(f_in)

        # with open(outputFileName, "wb") as f:
        #     for line in self.getLine(fileName, "rb"):
        #         f.write(gzip.compress(line))
        # with open(self.compressedOutputFileName, "wb") as fo:
        #     with open(self.outputFileName, "rb") as fi:
        #         fo.write(gzip.compress(fi))

    def compareCompress(self, skipOriginal=False):
        """Compare the original and the compressed file"""
        originalFileName = self.fileName
        newFileName = self.outputFileName
        originalCompressedName = str(self.fileName) + ".gz"
        newCompressedName = self.compressedOutputFileName

        if not skipOriginal:
            self.compressFile(originalFileName, originalCompressedName)
        self.compressFile(newFileName, newCompressedName)

        originalFileSize = self.getFileSize(originalCompressedName)
        finalFileSize = self.getFileSize(newCompressedName)

        assert self.getFileLines(newFileName) == self.getFileLines(originalFileName)
        print(f" compression ratio  = {originalFileSize/finalFileSize}")
        print(f"numeric values orginal : {originalFileSize} vs sort : {finalFileSize}")
        return originalFileSize / finalFileSize

    def printResults(self):
        print(f"Original File size : {self.getFileSize()}")
        print(f"Final    File size : {self.getFileSize(self.compressedOutputFileName)}")
        print(f"Original File lines : {self.getFileLines()}")
        print(f"Final    File lines : {self.getFileLines(self.outputFileName)}")

    def sortKmers(self, kmerOne: str, kmerTwo: str):
        """Sort the kmers in the right order for the minimiser"""
        return kmerOne < kmerTwo

    # Abstract methods
    @abstractmethod
    def reorganiseLines():
        pass

    def writeReorganisedLines(self, reorg_list):
        reorg_list = dict(zip([i for i in range(len(reorg_list))], reorg_list))
        readHolder = [None] * len(reorg_list)
        print("entered writer")
        with open(self.outputFileName, "w") as f:
            i = 0
            lastWritten = 0
            for line in self.LinesGenerator:
                #print("stocking line", line)
                index = reorg_list[i]
                readHolder[index] = line
                while lastWritten < len(readHolder) and readHolder[lastWritten]:
                    f.write(f"{readHolder[lastWritten]}\n")
                    #print("written line", readHolder[lastWritten], "readHolder has", sum([1 for item in readHolder if item]))
                    readHolder[lastWritten] = None
                    lastWritten += 1
                i += 1

    def doTheTrick(self, method= "", cover = None):
        reorg_list = self.reorganiseLines()
        self.writeReorganisedLines(reorg_list)
        ratio = self.compareCompress(False)
        with open(f"{method}_coli_{str(cover)}x_sizes.txt", "a") as f:
            f.write(str(ratio) + "\n")

        self.LinesGenerator = self.getLine()
        orgFile = self.getFileLines()
        self.LinesGenerator = self.getLine()
        reorgFile = self.getFileLines(self.outputFileName)

        print(f"Original File lines : {orgFile} ; Final File lines : {reorgFile}")
