from OrganizerMethod import OrganizerMethod
from ressources import minimiser

class LexMinMethod(OrganizerMethod):
    def __init__(
        self,
        fileName,
        outputFileName=None,
        minimiserSize=10,
        stopWhenOneCommonMin=True,
        maxReadsPerGroup=10,
    ):
        super().__init__(
            fileName,
            outputFileName,
        )
        
        # minimiser word size
        self.minimiserSize: int = minimiserSize

        self.maxReadsPerGroup: int = maxReadsPerGroup

        # stop the recursion when there is only one common minimiser for all reads
        self.stopWhenOneCommonMin: bool = stopWhenOneCommonMin


    
    def reorganiseLines(self, lineset: list = None, order=0) -> list:
        """Reorganise the lines of the file linked to this class using the lexicographic minimiser method.

        Parameters:
            lineset (optional): set of lines to be reorganised, will use self.LinesGenerator if none is supplied
            
            order (optional): The order of the minimiser to look at. For example, order = 0 will look at the most minimal of all, while order = 1 will look at the second most minimal.
            
        Returns:
            A list of the reorganised lineset. If it was called without lineset, then it will returns the whole reorganised file.
            """

        # if no lines was supplied, get the lines from the file. This is only the case for the first iteration.
        if lineset is None:
            lineset = self.LinesGenerator

        # stop condition
        elif len(lineset) < self.maxReadsPerGroup:
            return lineset

        minimiserToLines: dict = {}
        res: list() = []

        # Get the line minimiser
        for line in lineset:
            minimised = minimiser(line, self.minimiserSize, order)
            # Add the line to the dict, concat if already present otherwise it create a new entry
            minimiserToLines[minimised] = (
                minimiserToLines[minimised] + [line]
                if minimised in minimiserToLines
                else [line]
            )

        # Second stop condition
        if self.stopWhenOneCommonMin and len(minimiserToLines) == 1:
            return list(minimiserToLines.values())[0]

        # Go further in recursion, with an order + 1 (look at the next minimiser)
        for minimiserKey in minimiserToLines:
            res += self.reorganiseLines(minimiserToLines[minimiserKey], order + 1)

        return res

    def writeReorganisedLines(self, reorganised_lines: list) -> None:
        """Write the reorganised lines to the output file
        
        Parameters:
            
            reorganised_lines: the list of lines to write to self.outputFileName"""
        with open(self.outputFileName, "w") as f:
            # Switch to an iterator of reorganised lines
            for line in reorganised_lines:
                f.write(f"{line}\n")
