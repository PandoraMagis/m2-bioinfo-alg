from OrganizerMethod import OrganizerMethod
from vortex import VortexMethod
from kmerSplitter import kmerSplitter
from lexmin import LexMinMethod
from pathlib import Path
import argparse
import os

from ressources import Benchmarker

if __name__ == "__main__":
    print(" welcome in the read organiser on dev file")

    parser = argparse.ArgumentParser(
        prog="ProgramName",
        description="What the program does",
        epilog="Text at the bottom of help",
    )
    parser.add_argument(
        "-i",
        "--input-file",
        dest="inputfile",
        required=True,
        help="The .fasta or .fa file to be sorted.",
    )
    parser.add_argument("-o", "--output-file", help="The name of the output file")
    parser.add_argument(
        "-w",
        "--wordSize",
        help="""The size of the kmer used to sort the file with the selected method. The best size is set by default for each method.
        
            Meaning for each method:
                - Lexmin: minimizer size. Default is 8.
                - Vortex: vortex size. Default is 6.
                - Vortexmin: minimizer size. Default is 6."""
    )

    parser.add_argument("-c", "--cutoff", default=10, help="Only works with --method LexMin or --lexmin. Set the cutoff to stop the recursion when only this number of reads are in a group. Default is 10")

    parser.add_argument("-s", "--stopOneCommon", default=True, help="Only works with --method LexMin or --lexmin. Sets if the recursion stops when only one minimiser is present for the whole group. Default is True.")

    parser.add_argument("--vortex-window", help="The size of the vortex windows. Only works with --method VortexMin or --vm. Default is 50.")
    method_group = parser.add_mutually_exclusive_group()
    method_group.add_argument(
        "-m",
        "--method",
        help="Method used, default is lexicographic minimiser. Possible methods are : Vortex, LexMin, VortexMin, KmerSplit",
        default="lexmin",
    )
    method_group.add_argument(
        "-v",
        "--vortex",
        help="Use the vortex method. Similar to --method Vortex",
        action="store_true",
    )
    method_group.add_argument(
        "-l",
        "--lexmin",
        help="Use the lexicographic minimiser method. similar to --method LexMin",
        action="store_true",
    )
    method_group.add_argument(
        "--vm",
        "--vortexmin",
        help="Use the improved vortex with minimiser method. similar to --method VortexMin",
        action="store_true",
    )
    method_group.add_argument(
        "-k",
        "--kmersplit",
        help="Use the Kmer Splitter method. similar to --method KmerSplit",
        action="store_true",
    )
    args = parser.parse_args()

    LEXMIN_DEFAULT_WORDSIZE = 8
    VORTEX_DEFAULT_WORDSIZE = 6
    VORTEXMIN_DEFAULT_WORDSIZE = 6
    VORTEXMIN_DEFAULT_VORTEXWINDOW = 50

    output_file = args.output_file if args.output_file else os.path.splitext(args.input_file)[0] + "_reorg.fasta"

    if args.method.lower() == "lexmin" or args.lexmin:
        wordSize = int(args.wordSize) if int(args.wordSize) else LEXMIN_DEFAULT_WORDSIZE
        readOrg = LexMinMethod(args.input_file, outputFileName=output_file, minimiserSize=wordSize, stopWhenOneCommonMin=args.stopOneCommon, maxReadsPerGroup=int(args.cutoff))
    elif args.method.lower() == "vortex" or args.vortex:
        wordSize = int(args.wordSize) if int(args.wordSize) else VORTEX_DEFAULT_WORDSIZE
        readOrg = VortexMethod(args.input_file, outputFileName=output_file, naive=True, vortexSize=wordSize)
    elif args.method.lower() == "vortexmin" or args.vm:
        wordSize = int(args.wordSize) if int(args.wordSize) else VORTEXMIN_DEFAULT_WORDSIZE
        vortexWindow = args.vortexWindow if args.vortexWindow else VORTEX_DEFAULT_WORDSIZE
        readOrg = VortexMethod(args.input_file, outputFileName=output_file, naive=False, minimiserSize=wordSize, vortexWindowSize=vortexWindow)
    elif args.method.lower() == "kmersplit" or args.kmersplit:
        readOrg = kmerSplitter(args.inputfile, outputFileName=output_file)


    readOrg.doTheTrick()

    covers = [5,10,20,40,80,120]
    methods = ["vortexmin"]
    for method in methods:
        for cover in covers:
            for wordSize in range(3,15):
                with Benchmarker() as benchmark:
                    # print("Using minimiser size of", wordSize)
                    #readOrg = implemMy(Path(args.inputfile))
                    # humch1_1Mb_reads_40x.fasta
                    readOrg = VortexMethod(Path(f"projet/test/ecoli_100Kb_reads_{str(cover)}x.fasta"), naive=True if method=="vortex" else False, vortexSize=wordSize, vortexWindowSize=50, minimiserSize=wordSize)
                    # vortexParam : naive=True, vortexSize=wordSize, vortexWindowSize=50,
                    # readOrg = VortexMethod(Path(args.inputfile), vortexSize=50, minimiserSize=int(args.wordSize))
                    readOrg.doTheTrick(method, cover)

                bench = benchmark.results().split("\n")

                print(f"Duration: {bench[0]}s")
                with open(f"{method}_coli_{str(cover)}x_times.txt", "a") as tf:
                    tf.write(bench[0] + "\n")

                with open(f"{method}_coli_{str(cover)}x_ram.txt", "a") as mf:
                    mf.write(bench[1] + "\n")
    # with Benchmarker() as benchmark:
    #     # print("Using minimiser size of", wordSize)
    #     #readOrg = implemMy(Path(args.inputfile))
    #     wordSize = VORTEX_DEFAULT_WORDSIZE
    #     # humch1_1Mb_reads_40x.fasta
    #     readOrg = VortexMethod(Path(f"test/humch1_1Mb_reads_40x.fasta"), naive=True, vortexSize=wordSize, vortexWindowSize=50, minimiserSize=wordSize)
    #     # vortexParam : naive=True, vortexSize=wordSize, vortexWindowSize=50,
    #     # readOrg = VortexMethod(Path(args.inputfile), vortexSize=50, minimiserSize=int(args.wordSize))
    #     readOrg.doTheTrick("vortex", 40)

    # bench = benchmark.results().split("\n")

    # print(f"Duration: {bench[0]}s")
    # with open(f"vortex_human_40x_times.txt", "a") as tf:
    #     tf.write(bench[0] + "\n")

    # with open(f"vortex_human_40x_ram.txt", "a") as mf:
    #     mf.write(bench[1] + "\n")

    # list = readOrg.reorganiseLines()
    # readOrg.writeReorganisedLines(list)
    # readOrg.printResults()
