from itertools import count
import time
import math
#   PART ONE
def lazy() :
    time.sleep(0.00005)
    #return None

def computeTime(functionCore, functionTiming =lazy) :
    t0 = time.time()
    functionCore(functionTiming)
    print(time.time() - t0)


#################
#################
#################   Sorry but i'm too lazy to finish this file
#################
#################

NB_ELEMENT = 100

# computeTime(lambda x : x())
# computeTime(lambda x : [ x() for _ in range(NB_ELEMENT)])
# computeTime(lambda x : [[ x() for _ in range(NB_ELEMENT)]  for _ in range(NB_ELEMENT)])
# computeTime(lambda x : [ x() for _ in range(NB_ELEMENT*NB_ELEMENT*NB_ELEMENT)])
# computeTime(lambda x : [ x() for _ in range(math.log(NB_ELEMENT, 2))])


#   PART TWO
print("\n\npart two")
def stringPrint(S:str, a:int, b:int) :
    print(S[a:b+1])
    print(S[:a+1])
    print(S[b:])
    print(S[b:-1])
    print("".join([S[len(S)-(i+1)] for i in range(len(S))]))

stringPrint("testjnadpsamd", 5,7)


#   PART THREE
def areEquals(s1:str, s2:str):
    return len(s1) == len(s2) and sum([1 for i in range(len(s1)) if s1[i] != s2[i]]) == 0 

print(areEquals("test","test"))
print(areEquals("test","tes2t"))

def naivePaternMatching(patern:str, bank:str) :
    for counti, vali in enumerate(bank) :
        #   insta cut -> save one it
        if vali != patern[0] : continue
        #   main loop
        credY, ibis = 1, counti+1
        while ibis < len(bank) and credY < len(patern) and bank[ibis] == patern[credY] :
            ibis += 1 
            credY += 1
        if credY == len(patern) : print(counti)


naivePaternMatching("pat","treslepatpaterne")

def hashValue() : pass

def karpRabin(pat:str,bank:str,hashFunct:function) :

    # faire liste chainnée pour ca -> 

    pass

def kr(P: str, T: str) -> list:

    pos = []

    len_P = len(P)
    len_T = len(T)

    base_hash = setup_hash(P)
    hash = setup_hash(T[0 : len_P + 1])

    for i in range(len_T - len_P + 1):

        if i + len_P + 1 == len_T:
            break
        else:
            query = T[i : i + len_P]
            previous = query[0]
            next = T[i + len_P + 1]

        print(hash, base_hash)
        if hash == base_hash:

            if query == P:
                pos.append(i)

        hash = update_hash(hash, previous, next)

    return pos