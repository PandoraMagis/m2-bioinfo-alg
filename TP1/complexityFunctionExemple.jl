#   Exec One
function computeTime(coreFunction::Function, timefunction = ()-> sleep(0.00005)) 
    t0 = time()
    coreFunction(timefunction)
    return (time() - t0)
end

#   Time calculation
NB_ELEMENT = 100


computeTime( (f) -> f() )
computeTime( (f) -> [f() for _ in 0:NB_ELEMENT] )
computeTime( (f) -> [[f() for _ in 0:NB_ELEMENT] for _ in 0:NB_ELEMENT])
# computeTime( (f) -> [f() for _ in 0:*(NB_ELEMENT,NB_ELEMENT,NB_ELEMENT)] )
computeTime( (f) -> [f() for _ in 0:log2(NB_ELEMENT)] )
