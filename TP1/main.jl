# this file is a battele field where i debug and test thing

function computeTime(coreFunction::Function, timefunction = ()-> sleep(0.00005)) 
    t0 = time()
    coreFunction(timefunction)
    return (time() - t0)
end

#   Complexity in O(n-m)m  i.e. mostly n^2
function naivePaternMatching(patern::String, bank::String)
    strRes = []
    for i in 1:length(bank) # syntax usage problem that i don't get yet - see eachindex
        #   insta cut -> save one it
        bank[i] != patern[begin] ? continue : nothing
        #   main loop (ibisHotel)
        credY, ibis = 2, i+1
        while ibis < length(bank) && credY < length(patern) && bank[ibis] == patern[credY] 
            ibis += 1 
            credY += 1
        end
        credY == length(patern) ? append!(strRes, i) : ()
    end
    return strRes
end


#   Hash method
function hashValue(char::Char)::Int8
    # bit decalage
    return char == 'A' ? 0 :
        char == 'C' ? 1 :
        char == 'T' ? 2 :
        char == 'G' ? 3 :
        nothing # error
end

mutable struct StrDecompose
    head::Char
    tail::Union{StrDecompose, Nothing}

    StrDecompose(s::String) = length(s) > 1 ? new( s[begin]::Char,  StrDecompose(s[begin+1:end]) ) : new(s[begin], nothing) 
    # next()::Int = tail !== nothing ? hashValue<<2 : -1
end

function start(window::Int, strDec::StrDecompose)::Int
    return window == 0 ? hashValue(strDec.head) : (start(window-1,strDec.tail) + ( hashValue(strDec.head) << 2*(window) )) 
end

function karpRabin(patern::String, bank::StrDecompose)
    window = length(patern)-1
    cutInt = 2^((length(patern))*2)-1
    index = 1
    res = []

    pat = StrDecompose(patern)
    hashRef, hashVal = start(window, pat), 0

    #   load index to window hash val
    while window != 0
        hashVal = (hashVal << 2) + hashValue(bank.head)
        bank = bank.tail
        window -= 1
    end

    hashVal == hashRef ? println(index) : ()

    while bank !== nothing
        # println("index=$index ; val=$hashVal ;; char $(bank.head) = $(hashValue(bank.head))")
        hashVal = ((hashVal << 2) & cutInt) | hashValue(bank.head) # bit decale + bit or = decal 2 and add new result
        hashVal == hashRef ? append!(res, index) : ()
        bank = bank.tail
        index += 1

    end
    return res
end



import Random
pat = "AGT"
bank = Random.randstring(['A','T','G','C'], 10000)
println("patern compare")


naivePaternMatching(tuple::Tuple) = begin paton::String,bankon::String = tuple ; naivePaternMatching(paton, bankon) ; end
karpRabin(tuple::Tuple) = begin paton::String,bankon::StrDecompose = tuple ; karpRabin(paton, bankon) ; end


computeTime(naivePaternMatching, (pat,bank))
computeTime(karpRabin, (pat,StrDecompose(bank)))

karpRabin((pat,StrDecompose(bank)))