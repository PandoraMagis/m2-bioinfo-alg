#   Exec Two
function stringPrint(s::String, a::Int, b::Int)
    println(s[a:b+1])
    println(s[begin:a+1])
    println(s[b:end])
    println(s[b:end-1])
    println(join([s[end-(i-1)] for i in 1:length(s)]))
end

stringPrint("testjnadpsamd", 5,7)
